<?php

namespace App;
use \App\Medicamento;
use \App\Producto;
use \App\Movimiento;
use Illuminate\Database\Eloquent\Model;

/*
* Clase que identifica los distintos tipos de productos con su correspondiente codigo GTIN.
* Ej. Un Articulo puede tener muchos items con distintas fechas de expiracion en distintas farmacias.
*/
class Articulo extends Model
{
  protected $table = 'articulos';

  protected $fillable = [
      'id', 'nombre', 'gtin', 'tipo'
  ];

  public function items() {
    $user = \Auth::user();
    return $this->hasMany('App\Item')->where('farmacia_id', '=', \DB::raw($user->farmacia->id));
  }

  public function movimientos() {
    $user = \Auth::user();
    $articulo_id = $this->id;
    $farmacia_id = $user->farmacia_id;
    $results = Movimiento::select('movimientos.*')->join('items', function($join) use ($articulo_id, $farmacia_id) {
           $join->on('items.articulo_id', '=', \DB::raw($articulo_id));
           $join->on('items.farmacia_id', '=', \DB::raw($farmacia_id));
           $join->on('items.id', '=', 'movimientos.item_id');
       })
       ->where('movimientos.farmacia_id', '=', \DB::raw($farmacia_id))
       ->where('items.articulo_id', '=', \DB::raw($articulo_id))
       ->groupBy('movimientos.id')
       ->groupBy('items.id')
       //->orderBy('movimientos.item_id')
       ->orderBy('movimientos.id')
      ->get();
    return $results;
  }

  public function cantidad($farmacia_id = null) {
    $user = \Auth::user();
    $result = \DB::table('articulos')
        ->select(\DB::raw('COALESCE(SUM(items.cantidad), 0) as cantidad'))
        ->leftJoin('items', function($join) use ($user, $farmacia_id) {
             $join->on('articulos.id', '=', 'items.articulo_id');
             $join->on('items.farmacia_id', '=', $farmacia_id ? \DB::raw($farmacia_id) : \DB::raw($user->farmacia->id));
         })
        ->where('articulos.id', '=', $this->id)
        ->groupBy('articulos.id')
        ->first();
    return $result->cantidad;
  }

  public function getNewItem() {
    if ($this->tipo == config('constants.item.tipo.medicamento')) {
      $item = new Medicamento;
    } else {
      $item = new Producto;
    }
    $item->articulo_id = $this->id;
    return $item;
  }
}
