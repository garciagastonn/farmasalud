<?php

namespace App;

interface  Autorizacion {
  public static function autorizar($gtin, $fecha_vencimiento, $num_lote, $num_serie_produccion, $obra_social);
}
