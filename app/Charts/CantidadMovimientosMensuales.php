<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class CantidadMovimientosMensuales extends Chart
{
  private $colors = [1 => 'rgb(255, 99, 132)', 2 => 'rgb(255, 159, 64)', 3 => 'rgb(255, 205, 86)', 4 => 'rgb(75, 192, 192)', 5 => 'rgb(54, 162, 235)'];

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = \DB::table('movimientos')
              ->select('farmacia_id', \DB::raw('MONTH(created_at) as month'), \DB::raw('YEAR(created_at) as year'), \DB::raw('COUNT(movimientos.id) as cantidad'))
              ->whereRaw('created_at BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE()')
              ->groupBy('farmacia_id')
              ->groupBy(\DB::raw('MONTH(created_at)'))
              ->groupBy(\DB::raw('YEAR(created_at)'))
              ->get();
        $labels = array_unique(array_map(function($data) use ($meses){
          return $meses[$data->month] . ' ' . $data->year;
        }, $data->toArray()));
        $collection = collect($data->toArray());
        $grouped_movimientos = $collection->groupBy(function ($item, $key) {
            return $item->farmacia_id;
        });
        $this->labels($labels);
        foreach ($grouped_movimientos as $key => $movimientos_farmacia) {
          $values = array();
          foreach ($movimientos_farmacia as $j => $movimiento) {
            $values[] = $movimiento->cantidad;
          }
          $farmacia_id = $movimientos_farmacia[0]->farmacia_id;
          $this->dataset('Farmacia '. $farmacia_id, 'line', $values)->options(['borderColor' => $this->colors[$farmacia_id], 'fill' => false]);
        }
    }
}
