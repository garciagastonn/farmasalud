<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class CantidadVendidoPorEmpleado extends Chart
{
  private $colors = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $vendedores = \DB::table('users')
            ->select('users.id', 'users.nombre', 'users.farmacia_id', \DB::raw('COALESCE(SUM(movimientos.cantidad), 0) as ventas'))
            ->leftJoin('movimientos', function($join) {
                 $join->on('movimientos.user_id', '=', 'users.id');
                 $join->on('movimientos.evento', '=', \DB::raw(2));
             })
            ->groupBy('users.id')
            ->get();

            $labels = array_pad(array(), count($vendedores)+1, 0);
            $ventas = array();
            $ventas[1] = array_pad(array(), count($vendedores)+1, 0);
            $ventas[2] = array_pad(array(), count($vendedores)+1, 0);
            $ventas[3] = array_pad(array(), count($vendedores)+1, 0);
            $ventas[4] = array_pad(array(), count($vendedores)+1, 0);
            $ventas[5] = array_pad(array(), count($vendedores)+1, 0);
            foreach ($vendedores as $key => $vendedor) {
              $labels[$vendedor->id] = $vendedor->id . ': '. $vendedor->nombre;
              $ventas[$vendedor->farmacia_id][$vendedor->id] = $vendedor->ventas;
            }
            $this->labels($labels);

            $this->dataset('Farmacia 1', 'bar', $ventas[1])->backgroundColor($this->colors[0]);
            $this->dataset('Farmacia 2', 'bar', $ventas[2])->backgroundColor($this->colors[1]);
            $this->dataset('Farmacia 3', 'bar', $ventas[3])->backgroundColor($this->colors[2]);
            $this->dataset('Farmacia 4', 'bar', $ventas[4])->backgroundColor($this->colors[3]);
            $this->dataset('Farmacia 5', 'bar', $ventas[5])->backgroundColor($this->colors[4]);
    }
}
