<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class StockPorArticuloPorFarmacia extends Chart
{
  private $colors = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct($farmacia_id = null)
    {
        parent::__construct();
        $articulos = \DB::table('articulos')
            ->select('articulos.id', 'articulos.nombre', 'items.farmacia_id', \DB::raw('COALESCE(SUM(items.cantidad), 0) as cantidad'))
            ->leftJoin('items', function($join) use ($farmacia_id) {
                 $join->on('articulos.id', '=', 'items.articulo_id');
                 if ($farmacia_id) {
                   $join->on('items.farmacia_id', '=', \DB::raw($farmacia_id));
                 }
             })
            ->groupBy('articulos.id')
            ->groupBy('items.farmacia_id')
            ->orderBy('articulos.nombre')
            ->orderBy('items.farmacia_id')
            ->get();

            $collection = collect($articulos->toArray());
            $grouped_articulos = $collection->groupBy(function ($item, $key) {
                return $item->nombre;
            });
            $labels = array();
            $dataset_farmacia_1 = array();
            $dataset_farmacia_2 = array();
            $dataset_farmacia_3 = array();
            $dataset_farmacia_4 = array();
            $dataset_farmacia_5 = array();
            foreach ($grouped_articulos->toArray() as $nombre => $articulo_farmacias) {
              $labels[] = $articulo_farmacias[0]->id . ': ' . $nombre;
              $cantidades = array_pad (array() , 6 , 0);
              foreach ($articulo_farmacias as $key => $articulo_farmacia) {
                $cantidades[$articulo_farmacia->farmacia_id] = $articulo_farmacia->cantidad;
              }
              $dataset_farmacia_1[] = $cantidades[1];
              $dataset_farmacia_2[] = $cantidades[2];
              $dataset_farmacia_3[] = $cantidades[3];
              $dataset_farmacia_4[] = $cantidades[4];
              $dataset_farmacia_5[] = $cantidades[5];
            }
            $this->labels($labels); // articulos
            $this->dataset('Farmacia 1', 'bar', $dataset_farmacia_1)->backgroundColor($this->colors[0]);
            $this->dataset('Farmacia 2', 'bar', $dataset_farmacia_2)->backgroundColor($this->colors[1]);
            $this->dataset('Farmacia 3', 'bar', $dataset_farmacia_3)->backgroundColor($this->colors[2]);
            $this->dataset('Farmacia 4', 'bar', $dataset_farmacia_4)->backgroundColor($this->colors[3]);
            $this->dataset('Farmacia 5', 'bar', $dataset_farmacia_5)->backgroundColor($this->colors[4]);
    }


}
