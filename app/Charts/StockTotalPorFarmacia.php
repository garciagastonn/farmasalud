<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class StockTotalPorFarmacia extends Chart
{
  private $colors = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $stock_farmacias = \DB::table('farmacias')
            ->select('farmacias.id', 'farmacias.nombre', \DB::raw('COALESCE(SUM(items.cantidad), 0) as cantidad'))
            ->leftJoin('items', function($join) {
                 $join->on('items.farmacia_id', '=', 'farmacias.id');
             })
            ->groupBy('farmacias.id')
            ->get();

            $this->labels(['Farmacia 1', 'Farmacia 2', 'Farmacia 3', 'Farmacia 4', 'Farmacia 5']); // articulos
            $this->dataset('Stock total', 'pie', [$stock_farmacias[0]->cantidad, $stock_farmacias[1]->cantidad, $stock_farmacias[2]->cantidad, $stock_farmacias[3]->cantidad, $stock_farmacias[4]->cantidad])->backgroundColor([$this->colors[0],$this->colors[1], $this->colors[2], $this->colors[3], $this->colors[4]]);
    }
}
