<?php

namespace App;

use DB;
use App\Articulo;
use Illuminate\Database\Eloquent\Model;

use App\Charts\StockPorArticuloPorFarmacia;
use App\Charts\CantidadVendidoPorEmpleado;
use App\Charts\StockTotalPorFarmacia;
use App\Charts\CantidadMovimientosMensuales;

class Farmacia extends Model
{
  protected $table = 'farmacias';

  protected $fillable = [
      'id', 'nombre'
  ];

  public function usuarios() {
      return $this->hasMany('App\User');
  }

  public function items() {
      return $this->hasMany('App\Item');
  }

  public function getArticulos($filters) {
    $farmacia_id = $this->id;
    $perPage = 20;
    return Articulo::select(DB::raw('articulos.*, COALESCE(SUM(items.cantidad), 0) as cantidad'))
      ->leftJoin('items', function($join) use ($farmacia_id) {
           $join->on('articulos.id', '=', 'items.articulo_id');
           $join->on('farmacia_id', '=', DB::raw($farmacia_id));
       })
      ->groupBy('articulos.id')
      ->where($filters)
      ->paginate($perPage);
  }

  public function getReporte($farmacia_id) {
    $reporte = array();
    $reporte['chartSPAPF'] = new StockPorArticuloPorFarmacia($farmacia_id);
    $reporte['chartCVPE'] = new CantidadVendidoPorEmpleado();
    $reporte['chartSTPF'] = new StockTotalPorFarmacia();
    $reporte['chartCMM'] = new CantidadMovimientosMensuales();
    return $reporte;
  }
}
