<?php

namespace App\Http\Controllers;

use App\Articulo;
use Illuminate\Http\Request;

class ArticulosController extends Controller
{
  /**
   * Muestra el listado de productos y medicamentos.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $user = \Auth::user();
      $filters = [];
      if ($request) {
        $input = $request->all();
        if (count($input)) {
          unset($input['_token']);
          if (isset($input['page'])) {
            unset($input['page']);
          }
          foreach ($input as $filter => $value) {
            if (!is_null($value)) {
              $filters[$filter] = $value;
            }
          }
        }
      }
      $articulos = $user->farmacia->getArticulos($filters);
      return view('articulos.list', ['articulos' => $articulos, 'filters' => $filters]);
  }

  /**
  * Show the profile for the given user.
  *
  * @param  int  $id
  * @return Response
  */
  public function show($id)
  {
      $articulo = Articulo::findOrFail($id); // no se consulta por farmacia por que todos los articulos estan en cada farmacia (sin stock dado el caso)
      return view('articulos.detail', ['articulo' => $articulo, 'items' => $articulo->items, 'movimientos' => $articulo->movimientos()]);
  }
}
