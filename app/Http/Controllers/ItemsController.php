<?php

namespace App\Http\Controllers;

use App\Item;
use App\Articulo;
use App\Farmacia;
use App\Producto;
use App\Medicamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Muestra el listado de productos y medicamentos.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {

  }

  public function add(Request $request){
      $this->validate($request, [
          'articulo_id' => 'integer',
          'item_id' => 'integer',
          'event' => 'integer',
      ]);
      $user = Auth::user();
      $articulo_id = (int) $request->get('articulo_id', Articulo::whereIn('tipo', $user->articlesAllowedToSell())->first()->id); // si no viene por parametro agarro el primer articulo
      $item_id = (int) $request->get('id', null);
      $event = (int) $request->get('event', config('constants.item.add.pedidos'));
      $items = [];
      if ($articulo_id) {
        $items = Item::where(['farmacia_id' => $user->farmacia_id, 'articulo_id' => $articulo_id])->get();
      }
      $articulos = Articulo::whereIn('tipo', $user->articlesAllowedToSell())->get();
      return view('items.add', ['user' => $user, 'event' => $event, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'item_id' => $item_id]);
  }

  public function remove(Request $request){
    $this->validate($request, [
        'articulo_id' => 'integer',
        'item_id' => 'integer',
        'event' => 'integer',
    ]);
    $user = Auth::user();
    $articulo_id = (int) $request->get('articulo_id', Articulo::whereIn('tipo', $user->articlesAllowedToSell())->first()->id); // si no viene por parametro agarro el primer articulo
    $items = Item::where(['farmacia_id' => $user->farmacia_id, 'articulo_id' => $articulo_id])->get();
    $item_id = (int) $request->get('item_id', count($items) ? $items[0]->id : null);
    $item = $item_id ? Item::findOrFail($item_id) : null;
    $event = (int) $request->get('event', config('constants.item.remove.venta'));
    $articulos = Articulo::whereIn('tipo', $user->articlesAllowedToSell())->get();
    $articulo = Articulo::findOrFail($articulo_id); // articulo seleccionado
    return view('items.remove', ['user' => $user, 'event' => $event, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'selected_item' =>  $item, 'articulo' => $articulo]);
  }

  public function move(Request $request){
    $this->validate($request, [
        'articulo_id' => 'integer',
        'item_id' => 'integer',
        'farmacia_id' => 'integer',
        'event' => 'integer',
    ]);
    $user = Auth::user();
    $articulo_id = (int) $request->get('articulo_id', Articulo::whereIn('tipo', $user->articlesAllowedToSell())->first()->id);
    $items = Item::where(['farmacia_id' => $user->farmacia_id, 'articulo_id' => $articulo_id])->get();
    $item_id = (int) $request->get('item_id', count($items) ? $items[0]->id : null);
    $item = $item_id ? Item::findOrFail($item_id) : null;
    $event = (int) $request->get('event', config('constants.item.move'));
    $farmacia_origen_id = (int) $user->farmacia->id;
    $farmacias_destino = Farmacia::where('id', '<>', $farmacia_origen_id)->get();
    $farmacia_destino_id = (int) $request->get('farmacia_id', $farmacias_destino[0]->id);
    $articulos = Articulo::whereIn('tipo', $user->articlesAllowedToSell())->get();
    return view('items.move', ['farmacias_destino' => $farmacias_destino, 'user' => $user, 'event' => $event, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'selected_item' =>  $item, 'farmacia_destino_id' => $farmacia_destino_id]);
  }

    public function update(Request $request){
      $this->validate($request, [
          'cantidad' => 'required|integer',
          'event' => 'required|integer',
          'farmacia_id' => 'integer',
          'farmacia_origen_id' => 'integer',
          'farmacia_destino_id' => 'integer',
          'articulo_id' => 'required|integer',
          'item_id' => 'integer',
      ]);
      $articulo_id = (int) $request->get('articulo_id');
      $item_id = (int) $request->get('item_id');
      $event = (int) $request->get('event');
      $cantidad = (int) $request->get('cantidad');
      $farmacia_origen_id = (int) $request->get('farmacia_origen_id'); // solo mover
      $farmacia_destino_id = (int) $request->get('farmacia_destino_id'); // solo mover
      $obra_social = (int) $request->get('obra_social');

      $articulo = Articulo::findOrFail($articulo_id);
      if ($event == config('constants.item.add.pedidos')) {
          // donde se va a guardar los nuevos items pedidos al laboratorio
          $item = $articulo->getNewItem();
      } else {
          if ($articulo->tipo === config('constants.item.tipo.medicamento')) {
            $item = Medicamento::findOrFail($item_id);
          } else {
            $item = Producto::findOrFail($item_id);
          }
      }

      $item->actualizarStock($event, $cantidad, $farmacia_origen_id, $farmacia_destino_id, $obra_social);
      return redirect('/articulos/' . $articulo_id);
    }
}
