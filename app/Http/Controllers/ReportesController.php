<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Farmacia;

class ReportesController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Muestra los reportes del sistema.
   */
  public function index(Request $request)
  {
      $farmacia_usuario = \Auth::user()->farmacia;
      $farmacias = Farmacia::all();
      $farmacia_id = (int) $request->get('farmacia_id', null);
      $reporte = $farmacia_usuario->getReporte($farmacia_id);
      return view('reports', ['farmacias' => $farmacias, 'farmacia_id' => $farmacia_id, 'chartSPAPF' => $reporte['chartSPAPF'], 'chartCVPE' => $reporte['chartCVPE'], 'chartSTPF' => $reporte['chartSTPF'], 'chartCMM' => $reporte['chartCMM']]);
  }
}
