<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* Clase que identifica el stock disponible de un Articulo con su fecha de vencimiento en una farmacia
*/
class Item extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'id', 'cantidad', 'fecha_vencimiento', 'farmacia_id', 'articulo_id', 'num_serie_produccion', 'num_lote'
    ];

    public function farmacia()
    {
        return $this->belongsTo('App\Farmacia');
    }

    public function articulo()
    {
        return $this->belongsTo('App\Articulo');
    }

    protected function moverStock($farmacia_destino_id, $modificion_cantidad) {
      $this->cantidad = $this->cantidad - $modificion_cantidad;
      $item = Item::where(['farmacia_id' => $farmacia_destino_id, 'fecha_vencimiento' => $this->fecha_vencimiento,  'num_serie_produccion' => $this->num_serie_produccion,  'num_lote' => $this->num_lote, 'articulo_id' => $this->articulo_id])->first();
      if (is_null($item)) {
        $item = new Item;
        $item->farmacia_id = $farmacia_destino_id;
        $item->fecha_vencimiento = $this->fecha_vencimiento;
        $item->num_serie_produccion = $this->num_serie_produccion;
        $item->num_lote = $this->num_lote;
        $item->articulo_id = $this->articulo_id;
        $item->cantidad = $modificion_cantidad;

      } else {
        $item->cantidad = $item->cantidad + $modificion_cantidad;
      }
      $item->save();
      return $item;
    }

    protected function guardarMovimiento($event, $cantidad, $autorizacion_obra_social = '', $autorizacion_trazabilidad = '', $transaccion_laboratorio = '', $farmacia_destino_id = null, $item_moved = null) {
      $user_id = $user = \Auth::user()->id;
      if ($event === config('constants.item.move')) {
        /* guardo movimiento eliminar stock farmacia origen */
        $movimiento = new Movimiento;
        $movimiento->item_id = $this->id;
        $movimiento->cantidad = $cantidad;
        $movimiento->evento = config('constants.item.move_remove');
        $movimiento->user_id = $user_id;
        $movimiento->farmacia_id = $this->farmacia_id;
        $movimiento->autorizacion_obra_social = $autorizacion_obra_social;
        $movimiento->autorizacion_trazabilidad = $autorizacion_trazabilidad;
        $movimiento->transaccion_laboratorio = $transaccion_laboratorio;
        $movimiento->save();

        /* guardo movimiento agregar stock farmacia destino */
        $movimiento = new Movimiento;
        $movimiento->item_id = $item_moved->id;
        $movimiento->cantidad = $cantidad;
        $movimiento->evento = config('constants.item.move_add');
        $movimiento->user_id = $user_id;
        $movimiento->farmacia_id = $farmacia_destino_id;
        $movimiento->autorizacion_obra_social = $autorizacion_obra_social;
        $movimiento->autorizacion_trazabilidad = $autorizacion_trazabilidad;
        $movimiento->transaccion_laboratorio = $transaccion_laboratorio;
        $movimiento->save();
        return $movimiento;
      }
      $movimiento = new Movimiento;
      $movimiento->item_id = $this->id;
      $movimiento->cantidad = $cantidad;
      $movimiento->evento = (string) $event;
      $movimiento->user_id = $user_id;
      $movimiento->farmacia_id = $this->farmacia_id;
      $movimiento->autorizacion_obra_social = $autorizacion_obra_social;
      $movimiento->autorizacion_trazabilidad = $autorizacion_trazabilidad;
      $movimiento->transaccion_laboratorio = $transaccion_laboratorio;
      $movimiento->save();
      return $movimiento;
    }
}
