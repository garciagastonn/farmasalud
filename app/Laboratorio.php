<?php

namespace App;

class Laboratorio {

  public static function realizarPedido($gtin, $cantidad) {
    $faker = \Faker\Factory::create();
    return [
      'cantidad' => $cantidad,
      'fecha_vencimiento' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years'),
      'num_serie_produccion' => $faker->regexify('[A-Z0-9]{20}'),
      'num_lote' => $faker->regexify('[A-Z0-9]{20}'),
      'codigo_transaccion' => $faker->regexify('[A-Z0-9]{20}'),
    ];
  }

  public static function realizarDevolucion($gtin, $cantidad) {
    $faker = \Faker\Factory::create();
    return ['status' => true, 'codigo_transaccion' => $faker->regexify('[A-Z0-9]{20}')];
  }
}
