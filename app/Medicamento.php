<?php

namespace App;

use Log;
use Session;
use \App\Item;
use \App\Laboratorio;
use \App\ObraSocial;
use \App\Trazabilidad;
use \App\Movimiento;

class Medicamento extends Item {
  public function actualizarStock($event, $modificion_cantidad, $farmacia_origen_id = null, $farmacia_destino_id = null, $obra_social = null) {
      $user = \Auth::user();
      $autorizado = $user->canSellMedicine();
      $autorizacion_obra_social = '';
      $autorizacion_trazabilidad = '';
      $transaccion_laboratorio = '';
      $item_moved = null; // se usa en caso de mover stock entre farmacias
      if (!$autorizado) {
        Session::flash('error', 'Usuario no autorizado para vender medicamento');
        return false;
      }
      if ($event !== config('constants.item.move')) {
        $autorizacion_trazabilidad = Trazabilidad::autorizar($this->articulo->gtin, $this->fecha_vencimiento, $this->num_lote, $this->num_serie_produccion);
        if ($autorizacion_trazabilidad['status'] === false) {
          Session::flash('error', 'Modificacion de stock no autorizado por Trazabilidad');
          return false;
        } else {
          $autorizacion_trazabilidad = $autorizacion_trazabilidad['codigo_transaccion'];
        }
      }
      switch ($event) {
        case config('constants.item.add.pedidos'):
          $transaccion_laboratorio = $this->agregarStockPedidoMedicamentoLaboratorio($modificion_cantidad);
          break;
        case config('constants.item.add.devolucion'):
          $this->agregarStockDevolucionMedicamentoDeCliente($modificion_cantidad);
          break;
        case config('constants.item.remove.venta'):
          $autorizacion_obra_social = $this->quitarStockMedicamentoCompradoPorCliente($modificion_cantidad, $obra_social);
          break;
        case config('constants.item.remove.devolucion'):
          $transaccion_laboratorio = $this->quitarStockMedicamentoDevueltoALaboratorio($modificion_cantidad);
          break;
        case config('constants.item.move'):
          $item_moved = $this->moverStock($farmacia_destino_id, $modificion_cantidad);
          break;
      }
      try {
        $this->save();
        $log = "Modificacion en stock: evento {$event}, cantidad {$modificion_cantidad}, articulo #{$this->articulo_id}, item #{$this->id}";
        if ($event == config('constants.item.move')) {
          $log .= ", farmacia origen #{$farmacia_origen_id}, farmacia destino #{$farmacia_destino_id}";
        }
        Log::info($log);
        Session::flash('message', 'La modificacion de stock se realizo satisfactoriamente');
        $movimiento = $this->guardarMovimiento($event, $modificion_cantidad, $autorizacion_obra_social, $autorizacion_trazabilidad, $transaccion_laboratorio, $farmacia_destino_id, $item_moved);
      } catch (Exception $e) {
          report($e);
          Session::flash('error', 'Error en cambio de stock en medicamento');
          return false;
      }
  }

  private function agregarStockPedidoMedicamentoLaboratorio($modificion_cantidad) {
    $new_items_data = Laboratorio::realizarPedido($this->articulo->gtin, $modificion_cantidad);
    if ($new_items_data) {
      $user = \Auth::user();
      $this->cantidad = $new_items_data['cantidad'];
      $this->fecha_vencimiento = $new_items_data['fecha_vencimiento'];
      $this->num_serie_produccion = $new_items_data['num_serie_produccion'];
      $this->num_lote = $new_items_data['num_lote'];
      $this->farmacia_id = $user->farmacia_id;
      $transaccion_laboratorio = $new_items_data['codigo_transaccion'];
    }
    return $transaccion_laboratorio;
  }

  private function agregarStockDevolucionMedicamentoDeCliente($modificion_cantidad) {
    $this->cantidad = $this->cantidad + $modificion_cantidad;
  }

  private function quitarStockMedicamentoCompradoPorCliente($modificion_cantidad, $obra_social) {
    $autorizacion_obra_social = ObraSocial::autorizar($this->articulo->gtin, $this->fecha_vencimiento, $this->num_lote, $this->num_serie_produccion, $obra_social);
    if ($autorizacion_obra_social['status'] === false) {
      Session::flash('error', 'Modificacion de stock no autorizado por Obra Social');
      return false;
    } else {
      $autorizacion_obra_social = $autorizacion_obra_social['codigo_transaccion'];
    }
    $this->cantidad = $this->cantidad - $modificion_cantidad;
    return $autorizacion_obra_social;
  }

  private function quitarStockMedicamentoDevueltoALaboratorio($modificion_cantidad) {
    $status = Laboratorio::realizarDevolucion($this->articulo->gtin, $modificion_cantidad);
    $transaccion_laboratorio = $status['codigo_transaccion'];
    if ($status['status'] === false) {
      Session::flash('error', 'Modificacion de stock no autorizado por Laboratorio');
      return false;
    }
    $this->cantidad = $this->cantidad - $modificion_cantidad;
    return $transaccion_laboratorio;
  }
}
