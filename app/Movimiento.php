<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
  protected $table = 'movimientos';

  protected $fillable = [
      'id', 'item_id', 'evento', 'user_id', 'farmacia_id', 'cantidad', 'autorizacion_obra_social', 'autorizacion_trazabilidad', 'transaccion_laboratorio'
  ];
}
