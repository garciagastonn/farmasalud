<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisos extends Model
{
  protected $table = 'permisos';

  protected $fillable = [
      'id', 'name'
  ];

  public function usuarios() {
      return $this->belongsToMany('App\User', 'permisos_id');
  }
}
