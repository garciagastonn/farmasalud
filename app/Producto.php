<?php

namespace App;

use Log;
use Session;
use \App\Item;
use \App\Laboratorio;
use \App\ObraSocial;
use \App\Trazabilidad;
use \App\Movimiento;

class producto extends Item {
  public function actualizarStock($event, $modificion_cantidad, $farmacia_origen_id = null, $farmacia_destino_id = null, $obra_social = null) {
      $user = \Auth::user();
      $transaccion_laboratorio = '';
      $item_moved = null; // se usa en caso de mover stock entre farmacias
      switch ($event) {
        case config('constants.item.add.pedidos'):
          $transaccion_laboratorio = $this->agregarStockPedidoProductoLaboratorio($modificion_cantidad);
          break;
        case config('constants.item.add.devolucion'):
          $this->agregarStockDevolucionProductoDeCliente($modificion_cantidad);
          break;
        case config('constants.item.remove.venta'):
          $this->quitarStockProductoCompradoPorCliente($modificion_cantidad);
          break;
        case config('constants.item.remove.devolucion'):
          $transaccion_laboratorio = $this->quitarStockProductoDevueltoALaboratorio($modificion_cantidad);
          break;
        case config('constants.item.move'):
          $item_moved = $this->moverStock($farmacia_destino_id, $modificion_cantidad);
          break;
      }
      try {
        $this->save();
        $log = "Modificacion en stock: evento {$event}, cantidad {$modificion_cantidad}, articulo #{$this->articulo_id}, item #{$this->id}";
        if ($event == config('constants.item.move')) {
          $log .= ", farmacia origen #{$farmacia_origen_id}, farmacia destino #{$farmacia_destino_id}";
        }
        Log::info($log);
        Session::flash('message', 'La modificacion de stock se realizo satisfactoriamente');
        $this->guardarMovimiento($event, $modificion_cantidad, null, null, $transaccion_laboratorio, $farmacia_destino_id, $item_moved);
      } catch (Exception $e) {
          report($e);
          Session::flash('error', 'Error en cambio de stock en medicamento');
          return false;
      }
  }

  private function agregarStockPedidoProductoLaboratorio($modificion_cantidad) {
    $user = \Auth::user();
    $new_items_data = Laboratorio::realizarPedido($this->articulo->gtin, $modificion_cantidad);
    if ($new_items_data) {
      $this->cantidad = $new_items_data['cantidad'];
      $this->fecha_vencimiento = $new_items_data['fecha_vencimiento'];
      $this->num_serie_produccion = $new_items_data['num_serie_produccion'];
      $this->num_lote = $new_items_data['num_lote'];
      $this->farmacia_id = $user->farmacia_id;
      $transaccion_laboratorio = $new_items_data['codigo_transaccion'];
    }
    return $transaccion_laboratorio;
  }

  private function agregarStockDevolucionProductoDeCliente($modificion_cantidad) {
    $this->cantidad = $this->cantidad + $modificion_cantidad;
  }

  private function quitarStockProductoCompradoPorCliente($modificion_cantidad) {
    $this->cantidad = $this->cantidad - $modificion_cantidad;
  }

  private function quitarStockProductoDevueltoALaboratorio($modificion_cantidad) {
    $status = Laboratorio::realizarDevolucion($this->articulo->gtin, $modificion_cantidad);
    $transaccion_laboratorio = $status['codigo_transaccion'];
    if ($status['status'] === false) {
      Session::flash('error', 'Error en cambio de stock en medicamento');
      return false;
    }
    $this->cantidad = $this->cantidad - $modificion_cantidad;
    return $transaccion_laboratorio;
  }
}
