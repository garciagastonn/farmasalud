<?php

namespace App;
use App\Autorizacion;

class Trazabilidad implements Autorizacion {

  /*
  * TODO: verificar los parametros que recibe una autorizacion
   */
  public static function autorizar($gtin, $fecha_vencimiento, $num_lote, $num_serie_produccion, $obra_social = null) {
    $faker = \Faker\Factory::create();
    $args = array();
    $args[0]["f_evento"] = date("d-m-Y");
    $args[0]["h_evento"] = date("H:i", time());
    $args[0]["gln_origen"] = config('constants.gln_origen');
    $args[0]["gln_destino"] = config('constants.gln_destino');
    $args[0]["n_remito"] = $faker->regexify('[A-Z0-9]{10}');
    $args[0]["n_factura"] = rand(1300, 99999999);
    $args[0]["vencimiento"] = $fecha_vencimiento;
    $args[0]["gtin"] = $gtin;
    $args[0]["lote"] = $num_lote;
    $args[0]["numero_serial"] = $num_serie_produccion;
    $args[0]["id_evento"] = rand(1300, 99999999);
    $args[0]["n_postal"] = "1416";
    $args[0]["telefono"] = "45880712";

    // Envío de transacción al webserver de trazabilidad ANMAT
    $response = array('resultado' => true, 'codigoTransaccion' => $faker->regexify('[A-Z0-9]{20}')); //SendMedicamentos($args,$user,$pass);

    return array('status' => $response['resultado'], 'codigo_transaccion' => $response['codigoTransaccion']);
  }
}
