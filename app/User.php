<?php

namespace App;

use app\farmacia as Farmacia;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'email', 'password', 'farmacia_id', 'permisos_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function farmacia()
    {
        return $this->belongsTo('App\Farmacia');
    }

    public function permisos()
    {
        return $this->belongsTo('App\Permisos');
    }

    /*
    * debo mostrar notificaciones?
    */
    public function showNotifications()
    {
        return ($this->permisos_id == \Config::get('constants.permisos.director_tecnico'));
    }

    /*
    * usuario puede vender medicina o no?
    */
    public function canSellMedicine()
    {
        return (
          $this->permisos_id == \Config::get('constants.permisos.director_tecnico') ||
          $this->permisos_id == \Config::get('constants.permisos.auxiliar_farmaceutico')
        );
    }

    /*
    * array de productos que puede vender el usuario
    */
    public function articlesAllowedToSell()
    {
        return $this->canSellMedicine() ?
            [\Config::get('constants.item.tipo.producto'), \Config::get('constants.item.tipo.medicamento')] :
            [\Config::get('constants.item.tipo.producto')];
    }

    /*
    * obtener notificaciones
    */
    public function getNotifications()
    {
        $user = \Auth::user();
        $articulos_notificacion = [];
        if ($user->permisos_id == \Config::get('constants.permisos.director_tecnico')) {
          $faltantes_stock = \DB::table('articulos')
            ->select(\DB::raw('articulos.*, COALESCE(SUM(items.cantidad), 0) as cantidad'))
            ->leftJoin('items', function($join) use ($user) {
                 $join->on('articulos.id', '=', 'items.articulo_id');
                 $join->on('farmacia_id','=', \DB::raw($user->farmacia->id));
             })
            ->groupBy('articulos.id')
            ->orderBy('cantidad')
            ->orderBy('id')
            ->having('cantidad', '<', \Config::get('constants.item.cantidad_notificar'))
            ->get();
            $articulos_notificacion['faltantes_stock'] = $faltantes_stock;
            $proximos_a_vencer = \DB::table('articulos')
              ->select(\DB::raw('articulos.*, COALESCE(SUM(items.cantidad), 0) as cantidad'))
              ->leftJoin('items', function($join) use ($user) {
                   $join->on('articulos.id', '=', 'items.articulo_id');
                   $join->on('farmacia_id', '=', \DB::raw($user->farmacia->id));
                   $join->on('items.fecha_vencimiento','<', \DB::raw('DATE_ADD(CURDATE(), INTERVAL 7 DAY)'));
                   $join->on('items.cantidad', '>', \DB::raw(0));
               })
              ->groupBy('articulos.id')
              ->having(\DB::raw('COALESCE(SUM(items.cantidad), 0)'), '>', 0)
              ->get();
              $articulos_notificacion['proximos_a_vencer'] = $proximos_a_vencer;

        }
        return $articulos_notificacion;
    }
}
