<?php
  return [
    'permisos' => [
        'auxiliar' => '1',
        'auxiliar_farmaceutico' => '2',
        'director_tecnico' => '3',
    ],
    'item' => [
      'cantidad_notificar' => 5,
      'tipo' => [
        'producto' => 'producto',
        'medicamento' => 'medicamento',
      ],
      'add' => [
        'pedidos' => 0,
        'devolucion' => 1,
      ],
      'remove' => [
        'venta' => 2,
        'devolucion' => 3,
      ],
      'move' => 4,
      'move_add' => 5,
      'move_remove' => 6
    ],
    'gln_origen'  => 'glnws',
    'gln_destino' => 'GLN-1',
    'n_postal'    => '1416',
    'telefono'    => '45880712',
  ];
