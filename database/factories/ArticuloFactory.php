<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Articulo::class, function (Faker $faker) {
    return [
      'gtin' => $faker->regexify('[0-9]{20}'),
      'nombre' => ucwords(trim($faker->sentence(rand(1,3)), '.')),
      'tipo' => $faker->randomElement(['producto', 'medicamento']),
    ];
});
