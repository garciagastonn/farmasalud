<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'cantidad' => rand(0, 10),
        'farmacia_id' => null, // se pasa en la creacion por parametro
        'articulo_id' => rand(1,50), // uno de los 50 artuclos creados
        'fecha_vencimiento' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years'),
        'num_serie_produccion' => $faker->regexify('[A-Z0-9]{20}'),
        'num_lote' => $faker->regexify('[A-Z0-9]{20}'),
    ];
});
