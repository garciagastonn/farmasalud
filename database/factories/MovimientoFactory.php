<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
use \App\Item;
use \App\User;
$factory->define(App\Movimiento::class, function (Faker $faker) {
    $permisos = [
      'producto' => [config('constants.permisos.director_tecnico'), config('constants.permisos.auxiliar_farmaceutico'), config('constants.permisos.auxiliar')],
      'medicamento' => [config('constants.permisos.director_tecnico'), config('constants.permisos.auxiliar_farmaceutico')]
    ];
    $item = Item::inRandomOrder()->first();
    $usuario = User::where('farmacia_id', '=', $item->farmacia_id)->whereIn('permisos_id', $permisos[$item->articulo->tipo])->inRandomOrder()->first();
    return [
      'item_id' => $item->id,
      'farmacia_id' => $item->farmacia_id,
      'user_id' => $usuario->id,
      'cantidad' => rand(1,5),
      'evento' => (string) $faker->randomElement([config('constants.item.add.pedidos'), config('constants.item.add.devolucion'), config('constants.item.remove.venta'), config('constants.item.remove.devolucion')]),
      'autorizacion_obra_social' => $faker->regexify('[A-Z0-9]{20}'),
      'autorizacion_trazabilidad' => $faker->regexify('[A-Z0-9]{20}'),
      'transaccion_laboratorio' => $faker->regexify('[A-Z0-9]{20}'),
      'created_at' => $faker->dateTimeBetween($startDate = '-6 month', 'now'),
    ];
});
