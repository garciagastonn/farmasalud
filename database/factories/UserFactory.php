<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $name = $faker->firstName . ' ' . $faker->lastName;
    return [
        'nombre' => $name,
        'email' => strtolower(implode('.', explode(' ', $name))) . '@farmasalud.com.ar',
        'password' => Hash::make('123456'),
        'remember_token' => str_random(10),
        'farmacia_id' => null,
        'permisos_id' => null,
    ];
});
