<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cantidad');
            $table->date('fecha_vencimiento');
            $table->unsignedInteger('farmacia_id');
            $table->unsignedInteger('articulo_id');
            $table->string('num_serie_produccion');
            $table->string('num_lote');
            $table->timestamps();
            $table->foreign('farmacia_id')->references('id')->on('farmacias');
            $table->foreign('articulo_id')->references('id')->on('articulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
