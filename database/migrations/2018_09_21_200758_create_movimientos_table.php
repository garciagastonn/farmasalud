<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('farmacia_id');
            $table->unsignedInteger('user_id');
            $table->integer('cantidad');
            $table->enum('evento', [config('constants.item.add.pedidos'), config('constants.item.add.devolucion'), config('constants.item.remove.venta'), config('constants.item.remove.devolucion'), config('constants.item.move_add'), config('constants.item.move_remove')]);
            $table->string('autorizacion_obra_social')->nullable();
            $table->string('autorizacion_trazabilidad')->nullable();
            $table->string('transaccion_laboratorio')->nullable();
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('farmacia_id')->references('id')->on('farmacias');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
