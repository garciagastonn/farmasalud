<?php

use Illuminate\Database\Seeder;
use App\Farmacia as Farmacia;
use App\User as User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Creamos 50 articulos unicos
        $this->command->info('CREAR 50 ARTICULOS');
        factory(App\Articulo::class, 50)->create();

        // Actualmente, el grupo cuenta con 5 farmacias. Cada una está a cargo de
        // un farmacéutico con el cargo de Director Técnico, 2 farmacéuticos
        // auxiliares y 3 auxiliares de atención al público y despacho.

        // creamos 5 farmacias
        factory(Farmacia::class, 5)->create()->each(function ($farmacia) {
            $this->command->info('CREATE FARMACIA ' . $farmacia->id);
            // por cada farmacia creamos:
            // 1 director tecnico
            $this->command->info('CREATE USERS FARMACIA ' . $farmacia->id);
            $this->command->info('CREATE USER: DIRECTOR TECNICO');
            if ($farmacia->id === 1) {
              DB::table('users')->insert([
                'nombre' => 'Gaston Garcia',
                'email' => 'gaston.garcia@farmasalud.com.ar',
                'password' => Hash::make('123456'),
                'permisos_id' => Config::get('constants.permisos.director_tecnico'),
                'farmacia_id' => $farmacia->id,
                'remember_token' => str_random(10),
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
              ]);
            } else {
              $user = factory(App\User::class)->create([
                  'permisos_id' => Config::get('constants.permisos.director_tecnico'),
                  'farmacia_id' => $farmacia->id,
              ]);
            }


            // 2 farmacéuticos auxiliares
            $this->command->info('CREATE USER: 2 FARMACÉUTICOS AUXILIARES');
            $user = factory(App\User::class)->create([
                'permisos_id' => Config::get('constants.permisos.auxiliar_farmaceutico'),
                'farmacia_id' => $farmacia->id,
            ]);

            $user = factory(App\User::class)->create([
                'permisos_id' => Config::get('constants.permisos.auxiliar_farmaceutico'),
                'farmacia_id' => $farmacia->id,
            ]);

            // 3 auxiliares de atención al público y despacho
            $this->command->info('CREATE USER: 3 AUXILIARES');
            $user = factory(App\User::class)->create([
                'permisos_id' => Config::get('constants.permisos.auxiliar'),
                'farmacia_id' => $farmacia->id,
            ]);

            $user = factory(App\User::class)->create([
                'permisos_id' => Config::get('constants.permisos.auxiliar'),
                'farmacia_id' => $farmacia->id,
            ]);

            $user = factory(App\User::class)->create([
                'permisos_id' => Config::get('constants.permisos.auxiliar'),
                'farmacia_id' => $farmacia->id,
            ]);
            $cantidad = rand(50, 100);
            $this->command->info("CREATE {$cantidad} ITEMS EN STOCK");
            // crear entre 50 y 100 items (productos + medicamentos) por farmacia
            factory(App\Item::class, $cantidad)->create([
                'farmacia_id' => $farmacia->id,
            ]);
            $this->command->info("------------------------------");
        });
        // crear entre 5000 movimientos random para el historial y generar info para los reportes
        $this->command->info('CREATE: 5000 MOVIMIENTOS');
        factory(App\Movimiento::class, 5000)->create();
    }
}
