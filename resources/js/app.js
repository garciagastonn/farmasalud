
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('chart.js');
//require('bootstrap-select');
//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

/*const app = new Vue({
    el: '#app'
});*/

$(function() {
    // inicializa dropdown bootstrap
    $(".dropdown-toggle").dropdown();

    // inicializa tooltip bootstrap
    $('[data-toggle="tooltip"]').tooltip()

    // selectpicker
    //$('.selectpicker').selectpicker();

    // inicializa filtros de tabla listar articulos
    $('#filter-form').find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
            this.form.submit();
        }
    });
    $('#filter-form').find('select').change(function() {
        this.form.submit();
    });

    $('select.select-remove-stock').on('change', function() {
      var event = $(this).parents('form').find('input[name=event]').val();
      window.location = '/stock/eliminar?id=' + this.value+'&event=' + event;
    });

    // add stock farmacias
    $('select.select-articulo-add-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('event', $('form:visible').find('.event').val());
      params.set('articulo_id', this.value);
      params.delete('item_id');
      window.location.search = params.toString();
    });
    $('select.select-item-add-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('event', $('form:visible').find('.event').val());
      params.set('item_id', this.value);
      window.location.search = params.toString();
    });

    // remove stock farmacias
    $('select.select-articulo-remove-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('event', $('form:visible').find('.event').val());
      params.set('articulo_id', this.value);
      params.delete('item_id');
      window.location.search = params.toString();
    });
    $('select.select-item-remove-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      var articulo_id = $('.select-articulo-remove-items:visible').val();
      params.set('articulo_id', articulo_id);
      params.set('event', $('form:visible').find('.event').val());
      params.set('item_id', this.value);
      window.location.search = params.toString();
    });

    // mover stock farmacias
    $('#farmacia_destino').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('farmacia_id', this.value);
      window.location.search = params.toString();
    });
    $('select.select-articulo-move-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('articulo_id', this.value);
      params.delete('item_id');
      window.location.search = params.toString();
    });
    $('select.select-item-move-items').on('change', function() {
      var params = new URLSearchParams(location.search);
      params.set('item_id', this.value);
      window.location.search = params.toString();
    });

    // reportes
    $('.reportes-farmacias').click(function() {
      var params = new URLSearchParams(location.search);
      params.set('farmacia_id', this.value);
      window.location.search = params.toString();
    });
});
