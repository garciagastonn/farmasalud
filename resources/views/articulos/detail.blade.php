<?php
$eventos = ['Agregar Stock Pedidos Laboratorio', 'Agregar Stock Recibir devolución producto/s de cliente', 'Quitar producto comprado por cliente', 'Quitar producto devuelto al laboratorio', '', 'Mover Stock entre Farmacias: agregar stock destino', 'Mover Stock entre Farmacias: quitar stock origen'];
?>
@extends('layouts.app')

@section('content')
<div id="content" class="container">
          <div class="row justify-content-center">
            <div class="col-md-12 mb-2">

              <a href="{{ route('articulos') }}" title="Volver"><i class="fa fa-angle-left mb-4"></i> Volver</a>
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1>{{ $articulo->nombre }}</h1>
                @if ($articulo->tipo == \Config::get('constants.item.tipo.producto') || ($articulo->tipo == \Config::get('constants.item.tipo.medicamento') && \Auth::user()->canSellMedicine()))
                <div class="btn-toolbar mb-2 mb-md-0">
                  <a class="btn btn-sm btn-outline-success ml-1" role="button" aria-pressed="true" href="/items/agregar?articulo_id={{$articulo->id}}"><i class="fa fa-plus"></i> Agregar items</a>
                  <a class="btn btn-sm btn-outline-danger ml-1" role="button" aria-pressed="true" href="/items/eliminar?articulo_id={{$articulo->id}}"><i class="fa fa-times-circle"></i> Eliminar items</a>
                  <a class="btn btn-sm btn-outline-primary ml-1" role="button" aria-pressed="true" href="/items/mover?articulo_id={{$articulo->id}}"><i class="fa fa-exchange"></i> Mover items</a>
                </div>
                @endif
              </div>
            </div>
              <div class="col-md-4">
                <img src="holder.js/300x400?random=yes&text=Imagen {{ $articulo->tipo }}">
              </div>
              <div class="col-md-8">


                <div class="product-detail">

                  {{-- ID --}}
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">ID</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->id }}">
                    </div>
                  </div>

                  {{-- NOMBRE --}}
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Nombre</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->nombre }}">
                    </div>
                  </div>

                  {{-- TIPO --}}
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Tipo</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->tipo }}">
                    </div>
                  </div>

                  {{-- CANTIDAD --}}
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Cantidad</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->cantidad() }}">
                    </div>
                  </div>

                  {{-- codigo_comercial_gtin --}}
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Código Comercial GTIN</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->gtin }}">
                    </div>
                  </div>

                  {{-- FECHA CREACION --}}
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Fecha creación</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->created_at }}">
                    </div>
                  </div>


                  {{-- FECHA MODIFICACION --}}
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Fecha modificación</label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control-plaintext" value="{{ $articulo->updated_at }}">
                    </div>
                  </div>
                </div>
              </div>

                <div class="col-md-12 mt-5">
                  {{-- ITEMS --}}
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-1">
                    <h2>Items</h2>
                  </div>
                  <table class="table table-striped table-hover table-sm">
                    {{-- HEAD --}}
                    <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Cantidad</th>
                        <th># Lote</th>
                        <th># Serie Producción</th>
                        <th>Fecha Vencimiento</th>
                        <th>Última modificación</th>
                        <th></th>
                      </tr>
                    </thead>
                    {{-- end HEAD --}}
                    <tbody>
                      {{-- ITEMS --}}
                      @foreach ($items as $item)
                      <tr id="item_{{ $item->id }}">
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->cantidad }}</td>
                        <td>{{ $item->num_lote }}</td>
                        <td>{{ $item->num_serie_produccion }}</td>
                        <td>{{ $item->fecha_vencimiento }}</td>
                        <td>{{ $articulo->updated_at }}</td>
                        <td>
                          @if ($articulo->tipo == \Config::get('constants.item.tipo.producto') || ($articulo->tipo == \Config::get('constants.item.tipo.medicamento') && \Auth::user()->canSellMedicine()))
                          <div class="dropdown">
                            <button class="btn btn-outline-secondary dropdown-toggle btn-sm mt-2" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Acciones
                            </button>
                            <div class="dropdown-menu align-left" aria-labelledby="dropdownMenuButton2">
                              <a class="dropdown-item text-success" href="{{ route('items.add', ['articulo_id' => $articulo->id, 'item_id' => $item->id, 'event' => 1]) }}">
                                <i class="fa fa-plus mr-3"></i> Agregar Stock
                              </a>
                              <a class="dropdown-item text-primary" href="{{ route('items.remove', ['articulo_id' => $articulo->id, 'item_id' => $item->id]) }}">
                                <i class="fa fa-minus mr-3"></i> Quitar Stock
                              </a>
                              <a class="dropdown-item text-danger" href="{{ route('items.move', ['articulo_id' => $articulo->id, 'item_id' => $item->id]) }}">
                                <i class="fa fa-exchange mr-3"></i> Mover Stock
                              </a>
                            </div>
                          </div>
                          @endif
                        </td>
                      </tr>
                      @endforeach
                      {{-- end ITEMS --}}
                    </tbody>
                  </table>
                  {{-- end ITEMS --}}
                </div>

                  <div class="col-md-12 mt-3">
                    {{-- MOVIMIENTOS --}}
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-1">
                      <h2>Movimientos stock</h2>
                    </div>
                    <table class="table table-striped table-hover table-sm">
                      {{-- HEAD --}}
                      <thead class="thead-dark">
                        <tr>
                          <th>#</th>
                          <th>Item</th>
                          <th>Cantidad</th>
                          <th>Evento</th>
                          <th>autorizacion_obra_social</th>
                          <th>autorizacion_trazabilidad</th>
                          <th>transaccion_laboratorio</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                      {{-- end HEAD --}}
                      <tbody>
                        {{-- ITEMS --}}
                        @foreach ($movimientos as $movimiento)
                        <tr>
                          <td>{{ $movimiento->id }}</td>
                          <td><a href="#item_{{ $item->id }}">{{ $movimiento->item_id }}</a></td>
                          <td>{{ $movimiento->cantidad }}</td>
                          <th><a href="#" onclick="return false;" data-toggle="tooltip" data-placement="top" title="{{$eventos[intval($movimiento->evento)]}}" style="cursor: pointer;">{{ $movimiento->evento }}</a></th>
                          <th>{{ $movimiento->autorizacion_obra_social }}</th>
                          <th>{{ $movimiento->autorizacion_trazabilidad }}</th>
                          <th>{{ $movimiento->transaccion_laboratorio }}</th>
                          <th>{{ $movimiento->created_at }}</th>
                        </tr>
                        @endforeach
                        {{-- end ITEMS --}}
                      </tbody>
                    </table>
                    {{-- end MOVIMIENTOS --}}
                  </div>
      </div>

              <!-- end CONTENT -->

</div>

@endsection
