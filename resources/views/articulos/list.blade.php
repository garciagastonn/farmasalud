@extends('layouts.app')

@section('content')
<div id="content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5 border-bottom">
            <h1 class="h2">Articulos</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <?php echo count($filters) ? '<a href="/articulos" class="btn btn-sm btn-outline-secondary"><i class="fa fa-times-circle"></i> Borrar Filtros</a>' : '' ?>
            </div>
          </div>

          <!-- CONTENT -->
          <div class="content row">
              <div class="col-md-12">
                <!-- TABLE -->
                <div class="table-responsive">
                  <form id="filter-form" method="POST">
                    @csrf
                  <table class="table table-striped table-hover table-sm">

                    {{-- HEAD --}}
                    <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Cantidad</th>
                        <th>Última modificación</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    {{-- end HEAD --}}
                    <tbody>

                      {{-- FILTROS --}}

                      <tr>

                        <td><input name="articulo_id" type="text" class="form-control" placeholder="ID" value="{{isset($filters['articulo_id']) ? $filters['articulo_id']:''}}"></td>
                        <td><input name="nombre" type="text" class="form-control" placeholder="Nombre" value="{{isset($filters['nombre']) ? $filters['nombre']:''}}"></td>
                        <td><select name="tipo" class="form-control">
                          <option value="" selected></option>
                          <option {{ (isset($filters['tipo']) && $filters['tipo'] === 'Producto') ? 'selected':'' }} >Producto</option>
                          <option {{ (isset($filters['tipo']) && $filters['tipo'] === 'Medicamento') ? 'selected':'' }} >Medicamento</option>
                        </select></td>
                        <td><input name="cantidad" type="text" class="form-control" placeholder="Cantidad" value="{{isset($filters['cantidad']) ? $filters['cantidad']:''}}"></td>
                        <td><input name="updated_at" type="text" class="form-control" placeholder="2018-04-13" value="{{isset($filters['updated_at']) ? $filters['updated_at']:''}}"></td>
                        <th></th>
                        <th></th>
                      </tr>

                      {{-- end FILTROS --}}

                      {{-- ITEMS --}}
                      @foreach ($articulos as $articulo)
                      <tr>
                        <td>
                          <a class="dropdown-item text-success" href="/articulos/{{$articulo->id}}">
                            {{ $articulo->id }}
                          </a>
                        </td>
                        <td>{{ $articulo->nombre }}</td>
                        <td>{{ $articulo->tipo }}</td>
                        <td>{{ $articulo->cantidad }}</td>
                        <td>{{ $articulo->updated_at }}</td>
                        <td>
                          @if ($articulo->tipo == \Config::get('constants.item.tipo.producto') || ($articulo->tipo == \Config::get('constants.item.tipo.medicamento') && \Auth::user()->canSellMedicine()))
                          <div class="dropdown">
                            <button class="btn btn-outline-secondary dropdown-toggle btn-sm mt-2" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Acciones
                            </button>
                            <div class="dropdown-menu align-left" aria-labelledby="dropdownMenuButton2">
                              <a class="dropdown-item text-success" href="{{ route('items.add', ['articulo_id' => $articulo->id]) }}">
                                <i class="fa fa-plus mr-3"></i> Agregar Stock
                              </a>
                              <a class="dropdown-item text-primary" href="{{ route('items.remove', ['articulo_id' => $articulo->id]) }}">
                                <i class="fa fa-minus mr-3"></i> Quitar Stock
                              </a>
                              <a class="dropdown-item text-danger" href="{{ route('items.move', ['articulo_id' => $articulo->id]) }}">
                                <i class="fa fa-exchange mr-3"></i> Mover Stock
                              </a>
                            </div>
                          </div>
                          @endif
                        </td>
                        <th><a href="/articulos/{{ $articulo->id }}"><i class="fa fa-info-circle fa-2x mt-2"></i></a></th>


                      </tr>
                      @endforeach
                      {{-- end ITEMS --}}
                    </tbody>
                  </table>
                  </form>
                  {{ $articulos->links() }}
                </div>
            <!-- end TABLE -->
        </div>
      </div>
      <!-- end CONTENT -->
    </div>
  </div>
</div>
@endsection
