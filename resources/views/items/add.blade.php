@extends('layouts.app')

@section('content')
<div id="content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5 border-bottom">
            <h1 class="h2">Agregar Stock de Producto o Medicamento</h1>
          </div>

          <!-- CONTENT -->
          <div class="content row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-3">
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                      <a class="nav-link text-center @if ($event === Config::get('constants.item.add.pedidos')) active @endif" id="v-pills-pedidos-tab" data-toggle="pill" href="#v-pills-pedidos" role="tab" aria-controls="v-pills-pedidos" aria-selected="@if ($event === Config::get('constants.item.add.pedidos')) true @endif">
                          Agregar Stock Pedidos Laboratorio
                      </a>
                      <a class="nav-link text-center @if ($event === Config::get('constants.item.add.devolucion')) active @endif" id="v-pills-devolucion-tab" data-toggle="pill" href="#v-pills-devolucion" role="tab" aria-controls="v-pills-devolucion" aria-selected="@if ($event === Config::get('constants.item.add.devolucion')) false @endif">
                          Recibir Devolución Producto/s de Cliente
                      </a>
                  </div>
                </div>
                <div class="col-md-8 offset-md-1">
                  <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade @if ($event === config('constants.item.add.pedidos')) show active @endif" id="v-pills-pedidos" role="tabpanel" aria-labelledby="v-pills-pedidos-tab">
                        @include('items.add_form', ['event' => config('constants.item.add.pedidos'), 'user' => $user, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'item_id' => $item_id])
                    </div>
                    <div class="tab-pane fade @if ($event === config('constants.item.add.devolucion')) show active @endif" id="v-pills-devolucion" role="tabpanel" aria-labelledby="v-pills-devolucion-tab">
                        @include('items.add_form', ['event' => config('constants.item.add.devolucion'), 'user' => $user, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'item_id' => $item_id])
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- end CONTENT -->
        </div>
    </div>
</div>
@endsection
