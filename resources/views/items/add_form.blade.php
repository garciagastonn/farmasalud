<form method="POST" name="agregar" action="{{ route('items.update') }}" class="form{{$event}}" >
  @csrf
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5">
    @if ($event === config('constants.item.add.pedidos'))
      <h2 class="h2">Agregar Stock Pedidos Laboratorio</h2>
    @else
      <h2 class="h2">Recibir devolución producto/s de cliente</h2>
    @endif
  </div>

  <input class="event" type="hidden" name="event" value="{{$event}}">

  <div class="form-group row">
    <label for="selectFarmacia" class="col-sm-3 col-form-label">Farmacia</label>
    <div class="col-sm-9">
      <select name="farmacia_id" class="form-control" required readonly>
        <option value="{{$user->farmacia->id}}">#{{$user->farmacia->id}} {{$user->farmacia->nombre}}</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="selectArticulo" class="col-sm-3 col-form-label">Articulo</label>
    <div class="col-sm-9">
        <select name="articulo_id" class="articulo_id select-articulo-add-items form-control">
          @foreach ($articulos as $articulo)
              <option value="{{ $articulo->id }}" {{ $articulo->id == $articulo_id ? 'selected' : '' }}>{{$articulo->nombre}}: {{$articulo->tipo}}(Cantidad {{$articulo->cantidad()}})</option>
          @endforeach
        </select>

    </div>
  </div>

  @if ($event === config('constants.item.add.devolucion'))
  <div class="form-group row">
    <label for="selectItem" class="col-sm-3 col-form-label">Item</label>
    <div class="col-sm-9">
        <select name="item_id" class="item_id select-item-add-items form-control">
          @foreach ($items as $item)
              <option value="{{ $item->id }}" {{ $item->id == $item_id ? 'selected' : '' }}>{{$item->articulo->nombre}}: {{$item->num_serie_produccion}} (Candidad {{$item->cantidad}})</option>
          @endforeach
        </select>
    </div>
  </div>
  @endif

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Cantidad por agregar</label>
    <div class="col-sm-9">
      <input name="cantidad" class="cantidad form-control" type="number" value="1" min="1" max="9999" step="1" required/>
    </div>
  </div>

  <button type="submit" class="submit btn btn-primary btn-lg mt-4 pull-right">Agregar stock</button>
</form>
