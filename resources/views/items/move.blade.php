@extends('layouts.app')

@section('content')
<div id="content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5 border-bottom">
            <h1 class="h2">Mover Stock de Producto o Medicamento entre Farmacias</h1>
          </div>

          <!-- FORM -->

          <form method="POST" action="{{ route('items.update') }}" class="form-move">
            @csrf

            <input class="event" type="hidden" name="event" value="{{Config::get('constants.item.move')}}">

            <div class="form-group row">
              <label for="staticFarmacia" class="col-sm-3 col-form-label">Farmacia origen</label>
              <div class="col-sm-9">
                <select name="farmacia_origen_id" class="form-control" name="farmacia_origen" required readonly>
                  <option value="{{$user->farmacia->id}}">#{{$user->farmacia->id}}: {{$user->farmacia->nombre}}</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="staticFarmacia" class="col-sm-3 col-form-label">Farmacia destino</label>
              <div class="col-sm-9">
                <select name="farmacia_destino_id" id="farmacia_destino" class="form-control farmacia_destino" name="farmacia_destino" required>
                  @foreach ($farmacias_destino as $farmacia)
                      <option value="{{ $farmacia->id }}" {{ $farmacia->id == $farmacia_destino_id ? 'selected' : '' }}>#{{ $farmacia->id }}: {{ $farmacia->nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="selectArticulo" class="col-sm-3 col-form-label">Articulo</label>
              <div class="col-sm-9">
                  <select name="articulo_id" class="articulo_id select-articulo-move-items form-control">
                    @foreach ($articulos as $articulo)
                        <option value="{{ $articulo->id }}" {{ $articulo->id == $articulo_id ? 'selected' : '' }}>{{$articulo->nombre}} ({{$articulo->tipo}})</option>
                    @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="selectItem" class="col-sm-3 col-form-label">Item</label>
              <div class="col-sm-9">
                  @if (count($items))
                  <select name="item_id" class="item_id select-item-move-items form-control">
                    @foreach ($items as $item)
                        <option value="{{ $item->id }}" {{ $item->id == $selected_item->id ? 'selected' : '' }}>{{$item->articulo->nombre}}: {{$item->num_serie_produccion}} (Candidad {{$item->cantidad}})</option>
                    @endforeach
                  </select>
                  @else
                    <span>No hay items del articulo seleccionado</span>
                  @endif
              </div>
            </div>

            <div class="form-group row">
              <label for="inputPassword" class="col-sm-3 col-form-label">Cantidad por mover</label>
              <div class="col-sm-9">
                <input name="cantidad" class="cantidad form-control" type="number" value="{{ $selected_item->cantidad ?? 0 }}" min="0" max="{{ $selected_item->cantidad ?? 0 }}" step="1" required />
              </div>
            </div>

            <button type="submit" class="submit btn btn-primary btn-lg mt-4 pull-right" {{is_null($selected_item)? 'disabled' : ''}}>Mover stock</button>

          </form>


          <!-- end FORM -->
        </div>
    </div>
</div>
@endsection
