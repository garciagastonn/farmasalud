@extends('layouts.app')

@section('content')
<div id="content" class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5 border-bottom">
            <h1 class="h2">Eliminar Stock de Producto o Medicamento</h1>
          </div>

          <!-- CONTENT -->
          <div class="content row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-3">
                  <div class="nav flex-column nav-pills" id="v-pills-tab2" role="tablist" aria-orientation="vertical">

                      <a class="nav-link text-center @if ($event === Config::get('constants.item.remove.venta')) active @endif" id="v-pills-venta-tab" data-toggle="pill" href="#v-pills-venta" role="tab" aria-controls="v-pills-venta" aria-selected="@if ($event === Config::get('constants.item.remove.venta')) true @endif">
                          Quitar Producto Comprado por Cliente
                      </a>
                      <a class="nav-link text-center @if ($event === Config::get('constants.item.remove.devolucion')) active @endif" id="v-pills-devolucion2-tab" data-toggle="pill" href="#v-pills-devolucion2" role="tab" aria-controls="v-pills-devolucion2" aria-selected="@if ($event === Config::get('constants.item.remove.devolucion')) false @endif">
                          Quitar Producto Devuelto al Laboratorio
                      </a>
                  </div>
                </div>
                <div class="col-md-8 offset-md-1">
                  <div class="tab-content" id="v-pills-tabContent2">
                    <div class="tab-pane fade @if ($event === config('constants.item.remove.venta')) show active @endif" id="v-pills-venta" role="tabpanel" aria-labelledby="v-pills-venta-tab">
                      @include('items.remove_form', ['event' => config('constants.item.remove.venta'), 'user' => $user, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'selected_item' => $selected_item, 'articulo' => $articulo])
                    </div>
                    <div class="tab-pane fade @if ($event === config('constants.item.remove.devolucion')) show active @endif" id="v-pills-devolucion2" role="tabpanel" aria-labelledby="v-pills-devolucion2-tab">
                      @include('items.remove_form', ['event' => config('constants.item.remove.devolucion'), 'user' => $user, 'items' => $items, 'articulos' => $articulos, 'articulo_id' => $articulo_id, 'selected_item' => $selected_item])
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- end CONTENT -->
        </div>
    </div>
</div>
@endsection
