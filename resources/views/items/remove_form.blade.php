<form method="POST" name="agregar" action="{{ route('items.update') }}" class="form{{$event}}">
  @csrf
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5">
    @if ($event === config('constants.item.remove.venta'))
      <h2 class="h2">Quitar producto comprado por cliente</h2>
    @else
      <h2 class="h2">Quitar producto devuelto al laboratorio</h2>
    @endif
  </div>

  <input class="event" type="hidden" name="event" value="{{$event}}">

  <div class="form-group row">
    <label for="staticFarmacia" class="col-sm-3 col-form-label">Farmacia</label>
    <div class="col-sm-9">
      <select name="farmacia_id" class="farmacia_id form-control" name="farmacia" required readonly>
        <option value="{{$user->farmacia->id}}">#{{$user->farmacia->id}} {{$user->farmacia->nombre}}</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Articulo</label>
    <div class="col-sm-9">
        <select name="articulo_id" class="articulo_id select-articulo-remove-items form-control">
          @foreach ($articulos as $art)
              <option value="{{ $art->id }}" {{ $art->id == $articulo_id ? 'selected' : '' }}>{{$art->nombre}} ({{$art->tipo}})</option>
          @endforeach
        </select>

    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Item</label>
    <div class="col-sm-9">
      @if (count($items))
        <select name="item_id" class="item_id select-item-remove-items form-control">
          @foreach ($items as $item)
              <option value="{{ $item->id }}" {{ $item->id == $selected_item->id ? 'selected' : '' }}>{{$item->articulo->nombre}}: {{$item->num_serie_produccion}} (Candidad {{$item->cantidad}})</option>
          @endforeach
        </select>
      @else
        <span>No hay items del articulo seleccionado</span>
      @endif
    </div>
  </div>

  @if ($event == config('constants.item.remove.venta') && !is_null($articulo) && $articulo->tipo == 'medicamento')
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Número afiliado obra social</label>
    <div class="col-sm-9">
      <input name="obra_social" class="form-control obra_social" type="text" required />
    </div>
  </div>
  @endif

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Cantidad por eliminar</label>
    <div class="col-sm-9">
      <input name="cantidad" class="cantidad form-control" type="number" value="{{ $selected_item->cantidad ?? 0 }}" min="0" max="{{ $selected_item->cantidad ?? 0 }}" step="1" required />
    </div>
  </div>

  <button type="submit" class="btn btn-primary btn-lg mt-4 pull-right submit" {{is_null($selected_item)? 'disabled' : ''}}>Eliminar stock</button>
</form>
