<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/holder.min.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <!-- NAV -->
  <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('articulos') }}">
      <img class="mr-1" src="{{asset('img/logo.png')}}" alt="" width="18" height="18">
      <span>{{ config('app.name', 'Laravel') }}</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class=" pull-left">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="#" title="Farmacia #{{ Auth::user()->farmacia->id }}: {{ Auth::user()->farmacia->nombre }}">
            Farmacia #{{ Auth::user()->farmacia->id }}: "{{ Auth::user()->farmacia->nombre }}"
          </a>
        </li>
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">

        <!-- NOTIFICACIONES -->
        @if (Auth::user()->showNotifications())
        <?php $notifications = \Auth::user()->getNotifications() ?>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownNotif" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Notificaciones <span class="badge badge-light cantidad-notificaciones">{{count($notifications['faltantes_stock']) + count($notifications['proximos_a_vencer'])}}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right scrollable-menu" aria-labelledby="navbarDropdown">

            <h6 class="dropdown-header text-danger">Alerta de faltante de stock en los siguientes <span class="cantidad-notificaciones">{{count($notifications['faltantes_stock'])}}</span> items:</h6>
            @foreach ($notifications['faltantes_stock'] as $item)
            <a class="dropdown-item" href="/articulos/{{$item->id}}">
                {{ $item->nombre }} ({{ $item->cantidad }} disponibles)
            </a>
            @endforeach

            <h6 class="dropdown-header text-danger">Alerta de proximo vencimiento de stock. <span class="cantidad-notificaciones">{{count($notifications['proximos_a_vencer'])}}</span> items:</h6>
            @foreach ($notifications['proximos_a_vencer'] as $item)
            <a class="dropdown-item" href="/articulos/{{$item->id}}">
                {{ $item->nombre }} ({{ $item->cantidad }} por vencer)
            </a>
            @endforeach
          </div>
        </li>
        @endif
        <!-- end NOTIFICACIONES -->

        <!-- USUARIO LOGGEADO -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->nombre }}
          </a>

          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </li>
        <!-- end USUARIO LOGGEADO -->
      </ul>
    </div>
  </nav>

  <!-- SIDEBAR -->
  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link {{Route::current()->uri === 'articulos' ? 'active' : ''}}" href="{{ route('articulos') }}">
                <i class="fa fa-list-ul"></i>
                Listar productos
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Route::current()->uri === 'reportes' ? 'active' : ''}}" href="{{ route('reportes') }}">
                <i class="fa fa-bar-chart"></i>
                Generar Reportes
              </a>
            </li>
          </ul>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Modificar Stock</span>
            <a class="d-flex align-items-center text-muted" href="#">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link {{Route::current()->uri === 'items/agregar' ? 'active' : ''}}" href="{{ route('items.add') }}">
                <i class="fa fa-plus"></i>
                Agregar Stock
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Route::current()->uri === 'items/eliminar' ? 'active' : ''}}" href="{{ route('items.remove') }}">
                <i class="fa fa-minus"></i>
                Quitar producto
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Route::current()->uri === 'items/mover' ? 'active' : ''}}" href="{{ route('items.move') }}">
                <i class="fa fa-exchange"></i>
                Mover stock
              </a>
            </li>
          </ul>
        </div>
      </nav

      <main class="py-4">
          <div id="content" class="container">
            @if(Session::has('message'))
              <p class="alert alert-success mt-5" role="alert">
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </p>
            @endif
            @if(Session::has('error'))
              <p class="alert alert-danger" role="alert">
                {{ Session::get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </p>
            @endif
          </div>
          @yield('content')
          @yield('scripts')
      </main>
    </div>
  </div>
</body>
</html>
