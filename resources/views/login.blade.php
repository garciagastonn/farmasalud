<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ __('Farmasalud') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="<?php echo asset('css/app.css')?>" type="text/css">

    </head>
    <body class="text-center login">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
          @csrf

          <img class="mb-4" src="{{asset('img/logo.png')}}" alt="" width="120" height="120">
          <h1 class="h3 mb-3 font-weight-normal">{{ __('Farmasalud') }}</h1>

          <div class="form-group row">
              <label for="inputEmail" class="sr-only">{{ __('E-Mail Address') }}</label>
              <div class="col-md-12">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group row">
              <label for="inputPassword" class="sr-only">{{ __('Contraseña') }}</label>

              <div class="col-md-12">
                  <input id="inputPassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group row">
            <div class="col-md-12">
              <label>
                <input type="checkbox" value="remember-me" name="rememberme"> {{ __('Recuerdame') }}
              </label>
              <input class="btn btn-lg btn-primary btn-block" type="submit" value="Ingresar">
              <p class="mt-5 mb-3 text-muted text-center">{{ __('EFIP I - Garcia Gaston 2018') }}</p>
            </div>

          </div>
        </form>
    </body>
</html>
