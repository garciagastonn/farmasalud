@extends('layouts.app')

@section('content')
<div id="content" class="container reports">
    <div class="row justify-content-center">
        <div class="col-md-12">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-5 border-bottom">

            <h1 class="h2">Reportes</h1>

                <select name="farmacias" class="reportes-farmacias form-control pull-right" style="max-width: 200px">
                  <option value="0">Todas las farmacias</option>
                  @foreach ($farmacias as $farmacia)
                      <option value="{{ $farmacia->id }}" {{ $farmacia->id == $farmacia_id ? 'selected' : '' }}>#{{ $farmacia->id }} {{$farmacia->nombre}}</option>
                  @endforeach
                </select>

                <button id="print" type="button" class="btn btn-primary pull-right mr-1" onclick="window.print();">Imprimir</button>

            </div>
          </div>

          <!-- CONTENT -->
          <div class="content row mb-5">
            <div class="col-md-12">
              <h2 class="mb-5">Stock de articulos por farmacia</h2>
              <div class="graph">
                {!! $chartSPAPF->container() !!}
              </div>
            </div>

            <div class="col-md-12 mb-5">
              <h2 class="mb-5 mt-5">Cantidad vendido por empleado por farmacia</h2>
              <div class="graph">
                {!! $chartCVPE->container() !!}
              </div>
            </div>

            <div class="col-md-12 mb-5">
              <h2 class="mb-5 mt-5">Cantidad total stock por farmacia</h2>
              <div class="graph">
                {!! $chartSTPF->container() !!}
              </div>
            </div>

            <div class="col-md-12 mb-5">
              <h2 class="mb-5 mt-5">Cantidad de movimientos de stock mensuales por farmacia</h2>
              <div class="graph">
                {!! $chartCMM->container() !!}
              </div>
            </div>

          </div>
          <!-- end CONTENT -->
      </div>
    </div>
</div>


{!! $chartSPAPF->script() !!}
{!! $chartCVPE->script() !!}
{!! $chartSTPF->script() !!}
{!! $chartCMM->script() !!}
@endsection
