<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login', function () {
    return view('login');
});

Auth::routes();


Route::any('/articulos', 'ArticulosController@index')->name('articulos');
Route::any('/articulos/{id}', 'ArticulosController@show')->name('articulos.detalle');
Route::get('/items/agregar', 'ItemsController@add')->name('items.add');
Route::get('/items/eliminar', 'ItemsController@remove')->name('items.remove');
Route::get('/items/mover', 'ItemsController@move')->name('items.move');

Route::any('/items/update', 'ItemsController@update')->name('items.update');

Route::get('/reportes', 'ReportesController@index')->name('reportes');
