<?php

namespace Tests\Browser;

use Config;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Farmacia;
use App\Articulo;
use App\Item;
use App\User;
use App\Movimiento;
class AddStockTest extends DuskTestCase
{
    /*
    * Test Agregar Stock -> Pedidos Laboratorio -> Producto
    */
    public function testAgregarStockPedidosLaboratorioProducto()
    {
      $this->browse(function (Browser $browser) {

          $user = User::findOrFail(1);
          $articulo = Articulo::where('tipo', '=', 'producto')->firstOrFail();
          $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
          $cantidad_nuevos_items = rand(1,5);
          $browser->loginAs($user->farmacia_id)
              ->visit('/items/agregar')
              ->clickLink('Agregar Stock Pedidos Laboratorio')
              ->waitForText('Agregar Stock de Producto o Medicamento')
              ->assertSee('Agregar Stock Pedidos Laboratorio')
              ->assertSee('Farmacia')
              ->assertSee('Articulo')
              ->assertSee('Cantidad por agregar')
              ->select('form.form0 .articulo_id', $articulo->id)
              ->type('form.form0 .cantidad', $cantidad_nuevos_items)
              ->click('form.form0 .submit')
              ->waitForText($articulo->nombre); // pagina descripcion del articulo
          $this->assertTrue(($cantidad_original + $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
          $last_item = $this->checkNewItem($articulo, $user, $cantidad_nuevos_items);
          $this->checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_nuevos_items, Config::get('constants.item.add.pedidos'));
      });
    }

    /*
    * Test Agregar Stock -> Pedidos Laboratorio -> Medicamento
    */
    public function testAgregarStockPedidosLaboratorioMedicamento()
    {
      $this->browse(function (Browser $browser) {

          $user = User::findOrFail(1);
          $articulo = Articulo::where('tipo', '=', 'medicamento')->firstOrFail();
          $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
          $cantidad_nuevos_items = rand(1,5);
          $browser->loginAs($user->farmacia_id)
              ->visit('/items/agregar')
              ->clickLink('Agregar Stock Pedidos Laboratorio')
              ->waitForText('Agregar Stock de Producto o Medicamento')
              ->assertSee('Agregar Stock Pedidos Laboratorio')
              ->assertSee('Farmacia')
              ->assertSee('Articulo')
              ->assertSee('Cantidad por agregar')
              ->select('form.form0 .articulo_id', $articulo->id)
              ->waitForText('Agregar Stock de Producto o Medicamento')
              ->type('form.form0 .cantidad', $cantidad_nuevos_items)
              ->click('form.form0 .submit')
              ->waitForText($articulo->nombre); // pagina descripcion del articulo
          $this->assertTrue(($cantidad_original + $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
          $last_item = $this->checkNewItem($articulo, $user, $cantidad_nuevos_items);
          $this->checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_nuevos_items, Config::get('constants.item.add.pedidos'));
      });
    }

    /*
    * Test Agregar Stock -> Devolucion Cliente -> Producto
    */
    public function testAgregarStockDevolucionClienteProducto()
    {
      $this->browse(function (Browser $browser) {

          $user = User::findOrFail(1);
          $articulo = Articulo::where('tipo', '=', 'producto')->firstOrFail();
          $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->firstOrFail();
          $item_id = $item->id;
          $cantidad_original_item = (int) $item->cantidad;
          $cantidad_nuevos_items = rand(1,5);
          $browser->loginAs($user->farmacia_id)
              ->visit('/items/agregar')
              ->clickLink('Recibir Devolución Producto/s de Cliente')
              ->waitForText('Recibir devolución producto/s de cliente')
              ->assertSee('Recibir devolución producto/s de cliente')
              ->assertSee('Farmacia')
              ->assertSee('Articulo')
              ->assertSee('Item')
              ->assertSee('Cantidad por agregar')
              ->select('form.form1 .articulo_id', $articulo->id)
              ->waitForText('Recibir devolución producto/s de cliente')
              ->select('form.form1 .item_id', $item->id)
              ->waitForText('Recibir devolución producto/s de cliente')
              ->type('form.form1 .cantidad', $cantidad_nuevos_items)
              ->click('form.form1 .submit')
              ->waitForText($articulo->nombre); // pagina descripcion del articulo
          $item = Item::findOrFail($item_id);
          $this->assertTrue(($cantidad_original_item + $cantidad_nuevos_items) == $item->cantidad);
          $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.add.devolucion'));
      });
    }

    /*
    * Test Agregar Stock -> Devolucion Cliente -> Medicamento
    */
    public function testAgregarStockDevolucionClienteMedicamento()
    {
      $this->browse(function (Browser $browser) {

        $user = User::findOrFail(1);
        $articulo = Articulo::where('tipo', '=', 'medicamento')->firstOrFail();
        $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->firstOrFail();
        $cantidad_original_item = (int) $item->cantidad;
        $cantidad_nuevos_items = rand(1,5);
        $browser->loginAs($user->farmacia_id)
            ->visit('/items/agregar')
            ->clickLink('Recibir Devolución Producto/s de Cliente')
            ->waitForText('Recibir devolución producto/s de cliente')
            ->assertSee('Recibir devolución producto/s de cliente')
            ->assertSee('Farmacia')
            ->assertSee('Articulo')
            ->assertSee('Item')
            ->assertSee('Cantidad por agregar')
            ->select('form.form1 .articulo_id', $articulo->id)
            ->waitForText('Recibir devolución producto/s de cliente')
            ->select('form.form1 .item_id', $item->id)
            ->waitForText('Recibir devolución producto/s de cliente')
            ->type('form.form1 .cantidad', $cantidad_nuevos_items)
            ->click('form.form1 .submit')
            ->waitForText($articulo->nombre); // pagina descripcion del articulo
        $item2 = Item::findOrFail($item->id);
        $this->assertTrue(($cantidad_original_item + $cantidad_nuevos_items) == $item2->cantidad);
        $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.add.devolucion'));
      });
    }

    private function checkNewItem($articulo, $user, $cantidad_nuevos_items) {
      $last_item = Item::orderBy('updated_at', 'desc')->first();
      $this->assertTrue($last_item->articulo_id === $articulo->id);
      $this->assertTrue($last_item->farmacia_id === $user->farmacia_id);
      $this->assertTrue($last_item->cantidad === $cantidad_nuevos_items);
      return $last_item;
    }

    private function checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_nuevos_items, $evento) {
      $last_movimiento = Movimiento::orderBy('updated_at', 'desc')->first();
      $this->assertTrue($last_movimiento->item_id === $last_item->id);
      $this->assertTrue($last_movimiento->farmacia_id === $user->farmacia_id);
      $this->assertTrue($last_movimiento->user_id === $user->id);
      $this->assertTrue($last_movimiento->cantidad === $cantidad_nuevos_items);
      $this->assertTrue($last_movimiento->evento == $evento);
      if ($articulo->tipo == 'medicamento') {
        $this->assertTrue($last_movimiento->autorizacion_trazabilidad !== '');
      } else {
        $this->assertTrue($last_movimiento->autorizacion_trazabilidad === '' || is_null($last_movimiento->autorizacion_trazabilidad));
      }
      if ($last_movimiento->evento == Config::get('constants.item.add.pedidos')) {
        $this->assertTrue($last_movimiento->transaccion_laboratorio !== '');
      } else {
        $this->assertTrue($last_movimiento->transaccion_laboratorio === '' || is_null($last_movimiento->transaccion_laboratorio));
      }
      $this->assertTrue($last_movimiento->autorizacion_obra_social === '' || is_null($last_movimiento->autorizacion_obra_social));
    }
}
