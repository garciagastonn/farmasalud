<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Articulo;
use App\Item;

class DetailTest extends DuskTestCase
{
    /**
     * Este test prueba abrir la pagina correctamente
     *
     * @return void
     */
    public function testAbrirDetalleArticulo()
    {
        $this->browse(function (Browser $browser) {
            $articulo = Articulo::findOrFail(1);
            $browser->loginAs(User::findOrFail(1))
                ->visit('/articulos/1')
                ->waitForText($articulo->nombre)
                ->assertSee($articulo->nombre)
                ->assertSee('Items');
            $items = Item::where(['articulo_id' => $articulo->id]);
            foreach ($items as $key => $item) {
              $browser->assertSee($item->nombre);
            }
        });
    }
}
