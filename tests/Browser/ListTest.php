<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Articulo;
class ListTest extends DuskTestCase
{
    /**
     * Este test prueba abrir la pagina correctamente
     *
     * @return void
     */
    public function testAbrirPaginaCorrectamente()
    {
        $this->browse(function (Browser $browser) {
            $articulos = Articulo::take(10)->get();
            $browser->loginAs(User::find(1))
                ->visit('/articulos')
                ->assertSee('Articulos');
                foreach ($articulos as $key => $articulo) {
                  $browser->assertSee($articulo->nombre);
                }
        });
    }
}
