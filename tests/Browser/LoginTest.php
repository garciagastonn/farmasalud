<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * Loguear usuario en login form
     *
     * @return void
     */
    public function testAbrirPaginaCorrectamente()
    {
        $this->assertDatabaseHas('users', [
          'email' => 'gaston.garcia@farmasalud.com.ar'
        ]);
        $this->browse(function (Browser $browser) {

          $browser->visit('/')
            ->assertSee('Farmasalud')
            ->assertSee('EFIP I - Garcia Gaston 2018')
            ->type('email', 'gaston.garcia@farmasalud.com.ar')
            ->type('password', '123456')
            ->check('rememberme')
            ->press('Ingresar')
            ->assertPathIs('/articulos');
        });
    }
}
