<?php

namespace Tests\Browser;

use Config;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Farmacia;
use App\Articulo;
use App\Item;
use App\User;
use App\Movimiento;

class MoveStockTest extends DuskTestCase
{
    /**
     * Test Mover Stock
     */
    public function testMoveStockEntreFarmacias()
    {
        $this->browse(function (Browser $browser) {
          $user = User::findOrFail(1);
          $item = Item::where('farmacia_id', '=', $user->farmacia_id)->where('cantidad', '>', 1)->inRandomOrder()->firstOrFail();
          $articulo = Articulo::findOrFail($item->articulo_id);
          $farmacia_destino = Farmacia::where('id', '<>', $user->farmacia_id)->inRandomOrder()->firstOrFail();

          $cantidad_articulo_origen = (int) $articulo->cantidad($user->farmacia_id);
          $cantidad_articulo_destino = (int) $articulo->cantidad($farmacia_destino->id);
          $cantidad_item_original = (int) $item->cantidad;
          $cantidad_mover = rand(1, $item->cantidad);

          $browser->loginAs($user->farmacia_id)
              ->visit('/items/mover')
              ->assertSee('Mover Stock de Producto o Medicamento entre Farmacias')
              ->assertSee('Farmacia origen')
              ->assertSee('Farmacia destino')
              ->assertSee('Articulo')
              ->assertSee('Item')
              ->assertSee('Cantidad por mover')
              ->select('form.form-move .farmacia_destino', $farmacia_destino->id)
              ->assertSee('Mover Stock de Producto o Medicamento entre Farmacias')
              ->select('form.form-move .articulo_id', $item->articulo_id)
              ->assertSee('Mover Stock de Producto o Medicamento entre Farmacias')
              ->select('form.form-move .item_id', $item->id)
              ->assertSee('Mover Stock de Producto o Medicamento entre Farmacias')
              ->type('.cantidad', $cantidad_mover)
              ->click('.submit')
              ->waitForText($articulo->nombre); // pagina descripcion del articulo

          // verifico que se descuente en la cantidad en el item de la farmacia origen
          $item = Item::findOrFail($item->id);
          $this->assertTrue(($cantidad_item_original - $cantidad_mover) == $item->cantidad);
          $this->assertTrue(($cantidad_articulo_origen - $cantidad_mover) == $articulo->cantidad($user->farmacia_id));
          $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_mover, Config::get('constants.item.move_remove'));

          // verifico que se descuente en la cantidad en el item de la farmacia destino
          $last_item = $this->checkNewItem($articulo, $user, $cantidad_mover, $farmacia_destino->id);
          $this->assertTrue(($cantidad_articulo_destino + $cantidad_mover) == $articulo->cantidad($farmacia_destino->id));
          $this->checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_mover, Config::get('constants.item.move_add'), $farmacia_destino->id);
        });
    }

    private function checkNewItem($articulo, $user, $cantidad_nuevos_items, $farmacia_id) {
      $last_item = Item::orderBy('id', 'desc')->first();
      $this->assertTrue($last_item->articulo_id === $articulo->id);
      $this->assertTrue($last_item->farmacia_id === $farmacia_id);
      $this->assertTrue($last_item->cantidad === $cantidad_nuevos_items);
      return $last_item;
    }

    private function checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_nuevos_items, $evento, $farmacia_id = null) {
      $farmacia_id = $farmacia_id ?? $user->farmacia_id;
      $last_movimiento = Movimiento::where(['evento' => $evento, 'item_id' => $last_item->id])->orderBy('id', 'desc')->first();

      $this->assertTrue($last_movimiento->item_id === $last_item->id);
      $this->assertTrue($last_movimiento->farmacia_id === $farmacia_id);
      $this->assertTrue($last_movimiento->user_id === $user->id);
      $this->assertTrue($last_movimiento->cantidad === $cantidad_nuevos_items);
      $this->assertTrue($last_movimiento->evento == $evento);
      $this->assertTrue($last_movimiento->autorizacion_trazabilidad === '' || is_null($last_movimiento->autorizacion_trazabilidad));
      $this->assertTrue($last_movimiento->autorizacion_obra_social === '' || is_null($last_movimiento->autorizacion_obra_social));
      $this->assertTrue($last_movimiento->transaccion_laboratorio === '' || is_null($last_movimiento->transaccion_laboratorio));
    }
}
