<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Farmacia;
use App\Articulo;
use App\Item;
use App\User;
use App\Movimiento;
use Config;

class RemoveStockTest extends DuskTestCase
{
  /*
  * Test Eliminar Stock -> Comprado por Cliente -> Producto
  */
  public function testQuitarProductoCompradoPorClienteProducto()
  {
    $this->browse(function (Browser $browser) {

        $user = User::findOrFail(1);
        $articulo = Articulo::where('tipo', '=', 'producto')->firstOrFail();
        $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->where('cantidad', '>', '0')->firstOrFail();
        $cantidad_original_item = (int) $item->cantidad;
        $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
        $cantidad_nuevos_items = rand(1, $item->cantidad);
        $browser->loginAs($user->farmacia_id)
            ->visit('/items/eliminar')
            ->clickLink('Quitar Producto Comprado por Cliente')
            ->waitForText('Quitar producto comprado por cliente')
            ->assertSee('Quitar producto comprado por cliente')
            ->assertSee('Farmacia')
            ->assertSee('Articulo')
            ->assertSee('Item')
            ->assertSee('Cantidad por eliminar')
            ->select('form.form2 .articulo_id', $articulo->id)
            ->select('form.form2 .item_id', $item->id)
            ->type('form.form2 .cantidad', $cantidad_nuevos_items)
            ->click('form.form2 .submit')
            ->waitForText($articulo->nombre); // pagina descripcion del articulo

        $item = Item::findOrFail($item->id);
        $this->assertTrue(($cantidad_original_item - $cantidad_nuevos_items) == $item->cantidad);
        $this->assertTrue(($cantidad_original - $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
        $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.remove.venta'));
    });
  }

  /*
  * Test Eliminar Stock -> Comprado por Cliente -> Medicamento
  */
  public function testQuitarProductoCompradoPorClienteMedicamento()
  {
    $this->browse(function (Browser $browser) {

        $user = User::findOrFail(1);
        $articulo = Articulo::where('tipo', '=', 'medicamento')->firstOrFail();
        $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->where('cantidad', '>', '0')->firstOrFail();
        $cantidad_original_item = (int) $item->cantidad;
        $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
        $cantidad_nuevos_items = rand(1, $item->cantidad);
        $browser->loginAs($user->farmacia_id)
            ->visit('/items/eliminar')
            ->clickLink('Quitar Producto Comprado por Cliente')
            ->waitForText('Quitar producto comprado por cliente')
            ->assertSee('Quitar producto comprado por cliente')
            ->assertSee('Farmacia')
            ->assertSee('Articulo')
            ->assertSee('Item')
            ->assertSee('Cantidad por eliminar')
            ->select('form.form2 .articulo_id', $articulo->id)
            ->select('form.form2 .item_id', $item->id)
            ->waitForText('Quitar producto comprado por cliente')
            ->assertSee('Número afiliado obra social')
            ->type('form.form2 .obra_social', '123456789')
            ->type('form.form2 .cantidad', $cantidad_nuevos_items)
            ->click('form.form2 .submit')
            ->waitForText($articulo->nombre); // pagina descripcion del articulo
        $item = Item::findOrFail($item->id);
        $this->assertTrue(($cantidad_original_item - $cantidad_nuevos_items) == $item->cantidad);
        $this->assertTrue(($cantidad_original - $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
        $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.remove.venta'));
    });
  }

  /*
  * Test Eliminar Stock -> Devolucion Laboratorio -> Producto
  */
  public function testEliminarStockDevolucionLaboratorioProducto()
  {
    $this->browse(function (Browser $browser) {

        $user = User::findOrFail(1);
        $articulo = Articulo::where('tipo', '=', 'producto')->firstOrFail();
        $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->where('cantidad', '>', '0')->firstOrFail();
        $item_id = $item->id;
        $cantidad_original_item = (int) $item->cantidad;
        $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
        $cantidad_nuevos_items = rand(1,$item->cantidad);
        $browser->loginAs($user->farmacia_id)
            ->visit('/items/eliminar')
            ->clickLink('Quitar Producto Devuelto al Laboratorio')
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->assertSee('Quitar Producto Devuelto al Laboratorio')
            ->assertSee('Farmacia')
            ->assertSee('Articulo')
            ->assertSee('Item')
            ->assertSee('Cantidad por eliminar')
            ->select('form.form3 .articulo_id', $articulo->id)
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->select('form.form3 .item_id', $item->id)
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->type('form.form3 .cantidad', $cantidad_nuevos_items)
            ->click('form.form3 .submit')
            ->waitForText($articulo->nombre); // pagina descripcion del articulo
        $item = Item::findOrFail($item_id);
        $item = Item::findOrFail($item->id);
        $this->assertTrue(($cantidad_original_item - $cantidad_nuevos_items) == $item->cantidad);
        $this->assertTrue(($cantidad_original - $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
        $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.remove.devolucion'));
    });
  }

  /*
  * Test Eliminar Stock -> Devolucion Laboratorio -> Medicamento
  */
  public function testEliminarStockDevolucionLaboratorioMedicamento()
  {
    $this->browse(function (Browser $browser) {

        $user = User::findOrFail(1);
        $articulo = Articulo::where('tipo', '=', 'medicamento')->firstOrFail();
        $item = Item::where(['articulo_id' => $articulo->id, 'farmacia_id' => $user->farmacia_id])->where('cantidad', '>', '0')->firstOrFail();
        $item_id = $item->id;
        $cantidad_original_item = (int) $item->cantidad;
        $cantidad_original = (int) $articulo->cantidad($user->farmacia_id);
        $cantidad_nuevos_items = rand(1,$item->cantidad);
        $browser->loginAs($user->farmacia_id)
            ->visit('/items/eliminar')
            ->clickLink('Quitar Producto Devuelto al Laboratorio')
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->assertSee('Quitar Producto Devuelto al Laboratorio')
            ->assertSee('Farmacia')
            ->assertSee('Articulo')
            ->assertSee('Item')
            ->assertSee('Cantidad por eliminar')
            ->select('form.form3 .articulo_id', $articulo->id)
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->select('form.form3 .item_id', $item->id)
            ->waitForText('Quitar Producto Devuelto al Laboratorio')
            ->type('form.form3 .cantidad', $cantidad_nuevos_items)
            ->click('form.form3 .submit')
            ->waitForText($articulo->nombre); // pagina descripcion del articulo
        $item = Item::findOrFail($item->id);
        $this->assertTrue(($cantidad_original_item - $cantidad_nuevos_items) == $item->cantidad);
        $this->assertTrue(($cantidad_original - $cantidad_nuevos_items) == $articulo->cantidad($user->farmacia_id));
        $this->checkUltimoMovimiento($articulo, $item, $user, $cantidad_nuevos_items, Config::get('constants.item.remove.devolucion'));
    });
  }

  private function checkUltimoMovimiento($articulo, $last_item, $user, $cantidad_nuevos_items, $evento) {
    $last_movimiento = Movimiento::orderBy('updated_at', 'desc')->first();
    $this->assertTrue($last_movimiento->item_id === $last_item->id);
    $this->assertTrue($last_movimiento->farmacia_id === $user->farmacia_id);
    $this->assertTrue($last_movimiento->user_id === $user->id);
    $this->assertTrue($last_movimiento->cantidad === $cantidad_nuevos_items);
    $this->assertTrue($last_movimiento->evento == $evento);
    if ($articulo->tipo == 'medicamento') {
      $this->assertTrue($last_movimiento->autorizacion_trazabilidad !== '');
    } else {
      $this->assertTrue($last_movimiento->autorizacion_trazabilidad === '' || is_null($last_movimiento->autorizacion_trazabilidad));
    }
    if ($last_movimiento->evento == Config::get('constants.item.remove.venta')) {
      $this->assertTrue($last_movimiento->autorizacion_obra_social !== '');
    } else {
      $this->assertTrue($last_movimiento->autorizacion_obra_social === '' || is_null($last_movimiento->autorizacion_obra_social));
    }
    if ($last_movimiento->evento == Config::get('constants.item.remove.devolucion')) {
      $this->assertTrue($last_movimiento->transaccion_laboratorio !== '');
    } else {
      $this->assertTrue($last_movimiento->transaccion_laboratorio === '' || is_null($last_movimiento->transaccion_laboratorio));
    }
  }
}
